package com.ssnit.ssnit;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ssnit.ssnit.db.DBHelper;
import com.ssnit.ssnit.template.userTemplate;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyCard.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class MyCard extends android.support.v4.app.Fragment {

    private OnFragmentInteractionListener mListener;

    public MyCard() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_my_card, container, false);
        DBHelper dbHelper = new DBHelper(getActivity().getApplicationContext());
        userTemplate userTemplates = dbHelper.getCardInfo();
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView dateofbirth = (TextView) view.findViewById(R.id.dateofbirth);
        TextView sex  = (TextView) view.findViewById(R.id.sex);
        TextView membershipNo = (TextView) view.findViewById(R.id.membershipNo);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView3);

        Picasso.with(getContext().getApplicationContext())
                .load("http://ghanawater.co/ssnit/ssnit/uploads/"+userTemplates.profile_pic)
                .placeholder(R.drawable.avatar1)
                .error(R.drawable.avatar1)
                .into(imageView);

        String num = userTemplates.firstname + " "+ userTemplates.lastname;
        name.setText(num);
        sex.setText(userTemplates.sex);
        membershipNo.setText(userTemplates.id);
        dateofbirth.setText(userTemplates.issueDate);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
