package com.ssnit.ssnit.service;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.iid.InstanceIDListenerService;

public class MyIDListenerService extends InstanceIDListenerService {


    public MyIDListenerService() {
        super();
    }

    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        Log.e("First", "MyIDL");
        Intent intent = new Intent(this, IDListenerService.class);
        startService(intent);
    }

}
