package com.ssnit.ssnit;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ssnit.ssnit.api.apiCall;

public class Login extends AppCompatActivity {

    ProgressDialog progress;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(bundle!=null) {
                progress.dismiss();
                String status = bundle.getString("Status");
                Log.e("status", status);
                assert status != null;
                if(status.equals("313"))
                {
                    Toast.makeText(getApplicationContext(), "Invalid ID", Toast.LENGTH_LONG).show();
                }
                else
                {
                    SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("login", "1");
                    editor.apply();

                    Intent intent1 = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent1);
                    finish();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        registerReceiver(broadcastReceiver, new IntentFilter(apiCall.LOGIN));
        final EditText ssnitId = (EditText) findViewById(R.id.ssnitId);

        Button button = (Button) findViewById(R.id.login);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ssnitId.getText().toString().isEmpty())
                {
                    progress = new ProgressDialog(Login.this);
                    progress.setTitle("Login");
                    progress.setMessage("Logging in...");
                    progress.show();
                    apiCall apiCall = new apiCall(getApplicationContext());
                    apiCall.getUsers(ssnitId.getText().toString());
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        if(broadcastReceiver !=null)
        {
            unregisterReceiver(broadcastReceiver);
        }
        super.onDestroy();
    }
}
