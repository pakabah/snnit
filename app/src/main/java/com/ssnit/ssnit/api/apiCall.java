package com.ssnit.ssnit.api;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.ssnit.ssnit.db.DBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by pakabah on 10/04/16.
 */
public class apiCall {

    Context context;
    public static final String DEFAULT = "N/A";
    public static final String LOGIN = "android.intent.action.ssnit_login";
    public static final String RECORDS = "android.intent.action.ssnit_records";
    public static final String INFO = "android.intent.action.ssnit_info";

    public apiCall(Context context)
    {
        this.context = context;
    }

    public apiCall()
    {

    }


    public void getUsers(String id)
    {
        class altLogin extends AsyncTask<String,Void,String>
        {


            @Override
            protected String doInBackground(String... params) {
                String paramId = params[0];

                try {
                    URL url = new URL("http://ghanawater.co/api/ssnit/getUser");
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("id", paramId);
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();

                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (null != s && !s.isEmpty()) {
                    Log.e("Login Data", s);
                    DBHelper dbHelper = new DBHelper(context);
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        SharedPreferences sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("ssnit_id", jsonObject.getString("id"));
                        editor.apply();
                        dbHelper.insertUser(
                                jsonObject.getString("id"),jsonObject.getString("firstname"),
                                jsonObject.getString("lastname"),jsonObject.getString("othernames"),
                                jsonObject.getString("status"),jsonObject.getString("phone"),
                                jsonObject.getString("issueDate"),
                                jsonObject.getString("sex"),jsonObject.getString("profile_pic"));
                        publishLogin("DONE");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        altLogin altLogin = new altLogin();
        altLogin.execute(id);
    }

    public void sendRegistration(String token)
    {
        class altSendRegistration extends AsyncTask<String, Void,String>
        {

            @Override
            protected String doInBackground(String... params) {
                String paramId = params[0];

                try {
                    URL url = new URL("http://ghanawater.co/api/ssnit/registerDevice");
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("id", paramId);
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();

                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }
        }

        altSendRegistration altSendRegistration = new altSendRegistration();
        altSendRegistration.execute(token);

    }


    public void getRecords()
    {
        class altRecords extends AsyncTask<String,Void,String>
        {

            @Override
            protected String doInBackground(String... params) {

                try {
                    URL url = new URL("http://ghanawater.co/api/ssnit/getRecords");
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("id", getSSNITID());
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();

                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (null != s && !s.isEmpty()) {
                    Log.e("Records Data", s);
                    DBHelper dbHelper = new DBHelper(context);
                    dbHelper.deleteAllRecords();
                    try {
                        JSONArray jsonArray = new JSONArray(s);
                        for(int i=0; i < jsonArray.length(); i++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            dbHelper.insertRecords(jsonObject.getString("ssnit_id"),
                                    jsonObject.getString("company"),jsonObject.getString("amount"),
                                    jsonObject.getString("percentage"),jsonObject.getString("date"));
                            publishRecords();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        altRecords altRecords = new altRecords();
        altRecords.execute();
    }

    public void getInfo()
    {
     class altInfo extends AsyncTask<String,Void,String>
     {

         @Override
         protected String doInBackground(String... params) {
             try {
                 URL url = new URL("http://ghanawater.co/api/ssnit/getInfo");
                 HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                 connection.setRequestProperty("User-Agent", "");
                 connection.setRequestMethod("POST");
                 connection.setDoInput(true);

                 Uri.Builder builder = new Uri.Builder()
                         .appendQueryParameter("id", getSSNITID());
                 String query = builder.build().getEncodedQuery();

                 OutputStream os = connection.getOutputStream();
                 BufferedWriter writer = new BufferedWriter(
                         new OutputStreamWriter(os, "UTF-8"));
                 writer.write(query);
                 writer.flush();
                 writer.close();
                 os.close();

                 connection.connect();

                 InputStream inputStream = connection.getInputStream();
                 InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                 BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                 StringBuilder stringBuilder = new StringBuilder();
                 String bufferedStrChunk = null;

                 while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                     stringBuilder.append(bufferedStrChunk);
                 }
                 return stringBuilder.toString();
             } catch (IOException e) {
                 e.printStackTrace();
             }
             return null;
         }

         @Override
         protected void onPostExecute(String s) {
             super.onPostExecute(s);
             if (null != s && !s.isEmpty()) {
                 Log.e("Info Data", s);
                 DBHelper dbHelper = new DBHelper(context);
                 dbHelper.deleteAllInfo();
                 try {
                     JSONArray jsonArray = new JSONArray(s);
                     for(int i=0; i < jsonArray.length(); i++)
                     {
                         JSONObject jsonObject = jsonArray.getJSONObject(i);
                        dbHelper.insertInfo(jsonObject.getString("info"),jsonObject.getString("date"));
                         publishInfo();
                     }

                 } catch (JSONException e) {
                     e.printStackTrace();
                 }

             }
         }
     }

        altInfo altInfo = new altInfo();
        altInfo.execute();

    }

    public String IMEI()
    {
        TelephonyManager mngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return mngr.getDeviceId();
    }


    private void publishLogin(String Result)
    {
        Intent intent = new Intent(LOGIN);
        intent.setAction(LOGIN);
        intent.putExtra("Status", Result);
        Log.e("Login", "Broadcasting...");
        context.sendBroadcast(intent);
    }

    private void publishRecords()
    {
        Intent intent = new Intent(RECORDS);
        intent.setAction(RECORDS);
        intent.putExtra("Status", "DONE");
        Log.e("Records", "Broadcasting...");
        context.sendBroadcast(intent);
    }

    private void publishInfo()
    {
        Intent intent = new Intent(INFO);
        intent.setAction(INFO);
        intent.putExtra("Status", "DONE");
        Log.e("Info", "Broadcasting...");
        context.sendBroadcast(intent);
    }

    private String getSSNITID()
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
        return sharedPreferences.getString("ssnit_id", DEFAULT);
    }
}
