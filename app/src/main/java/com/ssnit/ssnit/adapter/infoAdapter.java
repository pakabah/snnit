package com.ssnit.ssnit.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ssnit.ssnit.R;
import com.ssnit.ssnit.template.infoTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by pakabah on 28/03/16.
 */
public class infoAdapter extends RecyclerView.Adapter<infoAdapter.ViewHolder> {

    List<infoTemplate> infoTemplates;
    Context context;

    public infoAdapter(List<infoTemplate> infoTemplate, Context context)
    {
        this.infoTemplates = new ArrayList<>();
        this.infoTemplates.addAll(infoTemplate);
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_info,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        infoTemplate infoTemplate = infoTemplates.get(position);

        SimpleDateFormat sim =new SimpleDateFormat("EEE, dd MMM yyyy");
        try {
            Date d = new SimpleDateFormat("dd-MM-yyyy").parse(infoTemplate.date);
            String newDate = sim.format(d);
            holder.day.setText(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        holder.dayish.setText(infoTemplate.dayish);
//        holder.month.setText(infoTemplate.month);
        holder.information.setText(infoTemplate.information);

    }

    @Override
    public int getItemCount() {
        return infoTemplates.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView day,dayish,month,information;
        CardView infoCard;
        public ViewHolder(View itemView) {
            super(itemView);
            day = (TextView) itemView.findViewById(R.id.day);
//            dayish = (TextView) itemView.findViewById(R.id.dayish);
//            month = (TextView) itemView.findViewById(R.id.month);
            information = (TextView) itemView.findViewById(R.id.infoData);
            infoCard = (CardView) itemView.findViewById(R.id.cardInfo);
        }
    }
}
