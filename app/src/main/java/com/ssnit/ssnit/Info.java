package com.ssnit.ssnit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ssnit.ssnit.adapter.infoAdapter;
import com.ssnit.ssnit.api.apiCall;
import com.ssnit.ssnit.db.DBHelper;
import com.ssnit.ssnit.template.infoTemplate;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Info.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class Info extends android.support.v4.app.Fragment {

    private OnFragmentInteractionListener mListener;

    public Info() {
        // Required empty public constructor
    }


    RecyclerView recyclerView;
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(bundle!=null) {

                String status = bundle.getString("Status");
                infoAdapter infoAdapter = new infoAdapter(initializeData(),getActivity().getApplicationContext());
                recyclerView.setAdapter(infoAdapter);
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().getApplicationContext().registerReceiver(broadcastReceiver, new IntentFilter(apiCall.INFO));
        View view = inflater.inflate(R.layout.fragment_info, container, false);
         recyclerView = (RecyclerView) view.findViewById(R.id.relInfo);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        infoAdapter infoAdapter = new infoAdapter(initializeData(),getActivity().getApplicationContext());
        recyclerView.setAdapter(infoAdapter);
        setHasOptionsMenu(true);
        return view;
    }

    public List<infoTemplate> initializeData()
    {
//        List<infoTemplate> infoTemplate = new ArrayList<>();
//        infoTemplate.add(new infoTemplate("30","th","Mar.","Payment Due"));
//        infoTemplate.add(new infoTemplate("30","th","Mar.","Payment Due"));

        DBHelper dbHelper = new DBHelper(getActivity().getApplicationContext());
        Log.e("Info Data", dbHelper.getInfo().toString());
        return dbHelper.getInfo();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.infoRefresh:
            apiCall apiCall = new apiCall(getActivity().getApplicationContext());
                apiCall.getInfo();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        if(broadcastReceiver !=null)
        {
            getActivity().getApplicationContext().unregisterReceiver(broadcastReceiver);
        }
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.info_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
