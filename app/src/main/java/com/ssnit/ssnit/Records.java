package com.ssnit.ssnit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ssnit.ssnit.adapter.recordAdapter;
import com.ssnit.ssnit.api.apiCall;
import com.ssnit.ssnit.db.DBHelper;
import com.ssnit.ssnit.template.recordTemplate;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Records.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class Records extends android.support.v4.app.Fragment {

    private OnFragmentInteractionListener mListener;

    public Records() {
        // Required empty public constructor
    }

    RecyclerView recyclerView;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(bundle!=null) {

                String status = bundle.getString("Status");
                Log.e("Records", "Broadcast Received");
                recordAdapter recordAdapter = new recordAdapter(initializeData(),getActivity().getApplicationContext());
                recyclerView.setAdapter(recordAdapter);
            }
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().getApplicationContext().registerReceiver(broadcastReceiver, new IntentFilter(apiCall.RECORDS));
        View view = inflater.inflate(R.layout.fragment_records, container, false);
         recyclerView = (RecyclerView) view.findViewById(R.id.relRecords);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recordAdapter recordAdapter = new recordAdapter(initializeData(),getActivity().getApplicationContext());
        recyclerView.setAdapter(recordAdapter);
        setHasOptionsMenu(true);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    public List<recordTemplate> initializeData()
    {
//        List<recordTemplate> recordTemplates = new ArrayList<>();
//        recordTemplates.add(new recordTemplate("30/03/16", "150GHC", "15%","myCompany365"));
//        recordTemplates.add(new recordTemplate("30/02/16", "150GHC", "15%","myCompany365"));
//        recordTemplates.add(new recordTemplate("30/01/16", "150GHC", "15%","myCompany365"));
//        recordTemplates.add(new recordTemplate("30/12/15", "150GHC", "15%","myCompany365"));
//        recordTemplates.add(new recordTemplate("30/11/15", "150GHC", "15%","myCompany365"));
//        recordTemplates.add(new recordTemplate("30/10/15", "150GHC", "15%","myCompany365"));
//        recordTemplates.add(new recordTemplate("30/09/15", "150GHC", "15%","myCompany365"));
//        recordTemplates.add(new recordTemplate("30/08/15", "150GHC", "15%","myCompany365"));
//        recordTemplates.add(new recordTemplate("30/07/15", "150GHC", "15%","myCompany365"));
//        recordTemplates.add(new recordTemplate("30/06/15", "150GHC", "15%","myCompany365"));
//        recordTemplates.add(new recordTemplate("30/05/15", "150GHC", "15%","myCompany365"));
//        return recordTemplates;
        DBHelper dbHelper = new DBHelper(getActivity().getApplicationContext());
        Log.e("Records Data", dbHelper.getRecords().toString());
        return dbHelper.getRecords();

    }


    @Override
    public void onDestroy() {
        if(broadcastReceiver !=null)
        {
            getActivity().getApplicationContext().unregisterReceiver(broadcastReceiver);
        }
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.recordsRefresh:
                apiCall apiCall = new apiCall(getActivity().getApplicationContext());
                apiCall.getRecords();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.records_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
